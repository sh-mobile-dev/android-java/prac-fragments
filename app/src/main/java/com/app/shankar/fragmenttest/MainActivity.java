package com.app.shankar.fragmenttest;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TableLayout;

public class MainActivity extends AppCompatActivity {

    private static final int NUM_PAGES = 3;
    private ViewPager mPager;
    private PagerAdapter mAdapter;
    TabLayout tabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPager=(ViewPager)findViewById(R.id.pager);
        mAdapter = new ScreenSlidePagerApapter(getSupportFragmentManager());
        mPager.setAdapter(mAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mPager);
    }

    @Override
    public void onBackPressed() {
        if(mPager.getCurrentItem()==0)
        {
            super.onBackPressed();
        }
        else
        {
            mPager.setCurrentItem(mPager.getCurrentItem()-1);
        }
    }

    private class ScreenSlidePagerApapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerApapter(FragmentManager fm)
        {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if(position==0){
                return new CheckFragment();
            }
            else if(position==1){
                return new CheckFragment2();
            }
            else {
                return new CheckFragment3();
            }


        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title;
            if(position==0)
            {
                title = "PAGE 1";
            }
            else if(position==1)
            {
                title = "PAGE 2";
            }
            else
            {
                title="PAGE 3";
            }
            return title;
        }
    }
}
